Public Class BrwRefBancarias

    Private Sub BrwRefBancarias_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If sw_refbanc = True Then
            sw_refbanc = False
            busca(0)
        End If
    End Sub

    Private Sub BrwRefBancarias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        busca(0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub busca(ByVal op As Integer)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Try

            If IsNumeric(Me.TextBox1.Text) = False Then
                Me.TextBox1.Text = 0
            End If

            con.Open()
            Me.BUSCA_Referencias_BancariasTableAdapter.Connection = con
            Me.BUSCA_Referencias_BancariasTableAdapter.Fill(Me.DataSetyahve.BUSCA_Referencias_Bancarias, CInt(Me.TextBox1.Text), Me.TextBox5.Text, Me.TextBox4.Text, Me.TextBox2.Text, op)
            con.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox4.Clear()
        Me.TextBox5.Clear()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            busca(1)
            Me.TextBox1.Clear()
        End If
    End Sub

    'Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    opcion = "N"
    '    FrmRefBancaria.Show()
    'End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.ContratoLabel1.Text) = True Then
            opcion = "C"
            Contrato = Me.ContratoLabel1.Text
            'Banco_RefBancaria = Me.Clv_BancoLabel1.Text
            FrmRefBan.Show()
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If IsNumeric(Me.ContratoLabel1.Text) = True Then
            opcion = "C"
            Contrato = Me.ContratoLabel1.Text
            'Banco_RefBancaria = Me.Clv_BancoLabel1.Text
            'FrmRefBancaria.Show()
            FrmRefBan.Show()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If IsNumeric(Me.ContratoLabel1.Text) = True Then
            opcion = "M"
            Contrato = Me.ContratoLabel1.Text
            'Banco_RefBancaria = Me.Clv_BancoLabel1.Text
            FrmRefBan.Show()
        End If
    End Sub

    
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busca(1)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        busca(2)
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        busca(3)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        busca(4)
    End Sub

   
End Class