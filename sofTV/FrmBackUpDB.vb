Imports System.Data.SqlClient

Public Class FrmBackUpDB
    Dim fecha As String = Nothing

    Private Sub HacerBackUp1(ByVal Ruta As String)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("HacerBackUp", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@RUTA", SqlDbType.VarChar, 300)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Ruta
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@RES", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@MSG", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            If CInt(parametro2.Value.ToString) = 1 Then
                MsgBox(parametro3.Value.ToString)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Dispose()
            conexion.Close()
        End Try

    End Sub
    Public Sub ChooseFolder()
        'If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
        ' TextBox1.Text = SaveFileDialog1.FileName
        'End If
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            TextBox1.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ChooseFolder()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If TextBox1.Text = "" Then
            MsgBox("Debe escribir una ruta correcta", MsgBoxStyle.OkOnly)
        Else
            HacerBackUp1(TextBox1.Text)
            MsgBox("El respaldo se genero correctamente en el servidor C:\Respaldos BD SQL Sof TV", MsgBoxStyle.OkOnly)
            Me.Close()
        End If
        Me.Close()

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub FrmBackUpDB_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub
End Class





