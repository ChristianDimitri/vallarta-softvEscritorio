
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Public Class FrmLeyendasEstadosCuentas

    Dim ley1 As String = ""
    Dim ley2 As String = ""
    Dim ley3 As String = ""
    Dim ley4 As String = ""


    Private Sub Guarda_cambios(ByVal leye1 As String, ByVal leye2 As String, ByVal leye3 As String, ByVal leye4 As String)



        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NUEVOGENERAL_ESTADO_DE_CUENTA", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Try
            conexion.Open()


            Dim parametro As New SqlParameter("@LEY1", SqlDbType.VarChar, 250)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = leye1
            comando.Parameters.Add(parametro)

            parametro = New SqlParameter("@LEY2", SqlDbType.VarChar, 250)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = leye2
            comando.Parameters.Add(parametro)

            parametro = New SqlParameter("@LEY3", SqlDbType.VarChar, 250)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = leye3
            comando.Parameters.Add(parametro)

            parametro = New SqlParameter("@LEY4", SqlDbType.VarChar, 250)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = leye4
            comando.Parameters.Add(parametro)

            comando.ExecuteNonQuery()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

        Finally
            conexion.Dispose()
            conexion.Close()

        End Try



    End Sub


    Private Sub Consulta()

        Dim CON01 As New SqlConnection(MiConexion)
        Dim SQL As New SqlCommand()
        Try
            CON01.Open()
            SQL = New SqlCommand()
            With SQL
                .CommandText = "CON_GENERAL_ESTADO_DE_CUENTA"
                .CommandTimeout = 0
                .Connection = CON01
                .CommandType = CommandType.StoredProcedure

                Dim reader As SqlDataReader = .ExecuteReader()

                While (reader.Read)
                    'if (reader.GetValue(0)) =  
                    Leyenda1.Text = reader.GetValue(0)
                    Leyenda2.Text = reader.GetValue(1)
                    Leyenda3.Text = reader.GetValue(2)
                    Leyenda4.Text = reader.GetValue(3)
                End While
            End With
            CON01.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

   

    Private Sub FrmLeyendasEstadosCuentas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Consulta()


    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        ley1 = Me.Leyenda1.Text
        ley2 = Me.Leyenda2.Text
        ley3 = Me.Leyenda3.Text
        ley4 = Me.Leyenda4.Text

        Guarda_cambios(ley1, ley2, ley3, ley4)

        Me.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Consulta()
    End Sub

    Private Sub Leyenda1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Leyenda1.KeyPress
        If e.KeyChar = Chr(13) Then
            e.KeyChar = Chr(0)
        End If
       

    End Sub

    Private Sub Leyenda1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Leyenda1.TextChanged

    End Sub

    Private Sub Leyenda2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Leyenda2.KeyPress
        If e.KeyChar = Chr(13) Then
            e.KeyChar = Chr(0)
        End If

    End Sub

    Private Sub Leyenda2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Leyenda2.TextChanged

    End Sub

    Private Sub Leyenda3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Leyenda3.KeyPress
        If e.KeyChar = Chr(13) Then
            e.KeyChar = Chr(0)
        End If

    End Sub

    Private Sub Leyenda3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Leyenda3.TextChanged

    End Sub

    Private Sub Leyenda4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Leyenda4.KeyPress
        If e.KeyChar = Chr(13) Then
            e.KeyChar = Chr(0)
        End If

    End Sub

    Private Sub Leyenda4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Leyenda4.TextChanged

    End Sub
End Class

