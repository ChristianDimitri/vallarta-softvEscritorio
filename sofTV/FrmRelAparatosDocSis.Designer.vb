﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelAparatosDocSis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cbcablemodem = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbdocsis = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.dgvrel = New System.Windows.Forms.DataGridView()
        Me.Clv_CableModem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MacCableModem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_DocSis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocSis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button3 = New System.Windows.Forms.Button()
        CType(Me.dgvrel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbcablemodem
        '
        Me.cbcablemodem.DisplayMember = "MacCableModem"
        Me.cbcablemodem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbcablemodem.FormattingEnabled = True
        Me.cbcablemodem.Location = New System.Drawing.Point(12, 46)
        Me.cbcablemodem.Name = "cbcablemodem"
        Me.cbcablemodem.Size = New System.Drawing.Size(262, 24)
        Me.cbcablemodem.TabIndex = 1
        Me.cbcablemodem.ValueMember = "Clv_CableModem"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(12, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 15)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Mac CableModem"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(289, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 15)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "DocSis"
        '
        'cbdocsis
        '
        Me.cbdocsis.DisplayMember = "Descripcion"
        Me.cbdocsis.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbdocsis.FormattingEnabled = True
        Me.cbdocsis.Location = New System.Drawing.Point(289, 46)
        Me.cbdocsis.Name = "cbdocsis"
        Me.cbdocsis.Size = New System.Drawing.Size(262, 24)
        Me.cbdocsis.TabIndex = 4
        Me.cbdocsis.ValueMember = "Clv_DocSis"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(557, 34)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&AGREGAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(557, 96)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 7
        Me.Button2.Text = "&ELIMINAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'dgvrel
        '
        Me.dgvrel.AllowUserToAddRows = False
        Me.dgvrel.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvrel.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvrel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvrel.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_CableModem, Me.MacCableModem, Me.Clv_DocSis, Me.DocSis})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvrel.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvrel.Location = New System.Drawing.Point(12, 96)
        Me.dgvrel.Name = "dgvrel"
        Me.dgvrel.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvrel.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvrel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvrel.Size = New System.Drawing.Size(539, 502)
        Me.dgvrel.TabIndex = 8
        '
        'Clv_CableModem
        '
        Me.Clv_CableModem.DataPropertyName = "Clv_CableModem"
        Me.Clv_CableModem.HeaderText = "Clv_CableModem"
        Me.Clv_CableModem.Name = "Clv_CableModem"
        Me.Clv_CableModem.ReadOnly = True
        Me.Clv_CableModem.Visible = False
        '
        'MacCableModem
        '
        Me.MacCableModem.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MacCableModem.DataPropertyName = "MacCableModem"
        Me.MacCableModem.HeaderText = "Mac CableModem"
        Me.MacCableModem.Name = "MacCableModem"
        Me.MacCableModem.ReadOnly = True
        '
        'Clv_DocSis
        '
        Me.Clv_DocSis.DataPropertyName = "Clv_DocSis"
        Me.Clv_DocSis.HeaderText = "Clv_DocSis"
        Me.Clv_DocSis.Name = "Clv_DocSis"
        Me.Clv_DocSis.ReadOnly = True
        Me.Clv_DocSis.Visible = False
        '
        'DocSis
        '
        Me.DocSis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DocSis.DataPropertyName = "Descripcion"
        Me.DocSis.HeaderText = "DocSis"
        Me.DocSis.Name = "DocSis"
        Me.DocSis.ReadOnly = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(557, 562)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 9
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'FrmRelAparatosDocSis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 612)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.dgvrel)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cbdocsis)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbcablemodem)
        Me.Name = "FrmRelAparatosDocSis"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Relación Aparatos - DocSis"
        CType(Me.dgvrel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbcablemodem As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbdocsis As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents dgvrel As System.Windows.Forms.DataGridView
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Clv_CableModem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MacCableModem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_DocSis As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocSis As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
