﻿Imports System.Data.SqlClient
Public Class FrmRelAparatosDocSis

    Private Sub FrmRelAparatosDocSis_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenacombos()
        llenadgvrel()
    End Sub
    Private Sub llenacombos()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0) '0 para combo cablemodems
        cbcablemodem.DataSource = BaseII.ConsultaDT("MuestraAparatosDocsis")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1) '1 para combo docsis
        cbdocsis.DataSource = BaseII.ConsultaDT("MuestraAparatosDocsis")
    End Sub
    Private Sub llenadgvrel()
        BaseII.limpiaParametros()
        dgvrel.DataSource = BaseII.ConsultaDT("MuestraRelAparatosDocsis")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If cbcablemodem.Text = "" Then
            MsgBox("Selecciona una Mac de CableModem")
            Exit Sub
        End If
        If cbdocsis.Text = "" Then
            MsgBox("Selecciona un DocSis")
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@clv_cablemodem", SqlDbType.Int, cbcablemodem.SelectedValue)
            BaseII.CreateMyParameter("@clv_docsis", SqlDbType.Int, cbdocsis.SelectedValue)
            BaseII.Inserta("AgregaEliminaRelAparatoDocsis")
            llenacombos()
            llenadgvrel()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If dgvrel.Rows.Count = 0 Then
            Exit Sub
        End If
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@opcion", SqlDbType.Int, 1)
            BaseII.CreateMyParameter("@clv_cablemodem", SqlDbType.Int, dgvrel.SelectedCells(0).Value)
            BaseII.CreateMyParameter("@clv_docsis", SqlDbType.Int, dgvrel.SelectedCells(2).Value)
            BaseII.Inserta("AgregaEliminaRelAparatoDocsis")
            llenacombos()
            llenadgvrel()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub
End Class