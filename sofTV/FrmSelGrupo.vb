Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelGrupo

    Private Sub ConSelGrupoPro(ByVal Clv_Session As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("Exec ConSelGrupoPro ")
        strSQL.Append(CStr(Clv_Session) & ", ")
        strSQL.Append(CStr(Op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ListBox1.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConSelGrupoTmp(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("Exec ConSelGrupoTmp ")
        strSQL.Append(CStr(Clv_Session))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.ListBox2.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub InsertarSelGrupoTmp(ByVal Clv_Session As Long, ByVal Clv_Grupo As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertarSelGrupoTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Grupo", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Grupo
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Op", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Op
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub EliminarSelGrupoTmp(ByVal Clv_Session As Long, ByVal Clv_Grupo As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("EliminarSelGrupoTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clv_Grupo", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clv_Grupo
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Op", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Op
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        InsertarSelGrupoTmp(LocClv_session, CInt(Me.ListBox1.SelectedValue), 0)
        ConSelGrupoPro(LocClv_session, 1)
        ConSelGrupoTmp(LocClv_session)
    End Sub

    Private Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        InsertarSelGrupoTmp(LocClv_session, 0, 1)
        ConSelGrupoPro(LocClv_session, 1)
        ConSelGrupoTmp(LocClv_session)
    End Sub

    Private Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        EliminarSelGrupoTmp(LocClv_session, CInt(Me.ListBox2.SelectedValue), 0)
        ConSelGrupoPro(LocClv_session, 1)
        ConSelGrupoTmp(LocClv_session)
    End Sub

    Private Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        EliminarSelGrupoTmp(LocClv_session, 0, 1)
        ConSelGrupoPro(LocClv_session, 1)
        ConSelGrupoTmp(LocClv_session)
    End Sub

    Private Sub FrmSelGrupo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        ConSelGrupoPro(LocClv_session, 0)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If Me.ListBox2.Items.Count = 0 Then
            MsgBox("Selecciona al menos un Grupo de Ventas.", MsgBoxStyle.Information)
            Exit Sub
        End If
        FrmSelFechas.Show()
        Me.Close()
    End Sub
End Class