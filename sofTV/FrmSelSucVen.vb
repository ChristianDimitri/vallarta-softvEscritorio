Imports System.Data.SqlClient
Public Class FrmSelSucVen

    Private Sub FrmSelSucVen_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim COn As New SqlConnection(MiConexion)
        COn.Open()
        Me.ConSucursalTableAdapter.Connection = COn
        Me.ConSucursalTableAdapter.Fill(Me.DataSetEric.ConSucursal)
        COn.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.NombreComboBox.SelectedValue > 0 Then
            eClv_Sucursal = Me.NombreComboBox.SelectedValue
            eNombre = ""
            eNombre = Me.NombreComboBox.Text
            FrmImprimirComision.Show()
            Me.Close()
        End If
    End Sub
End Class