<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSeleccionaTel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Label1 As System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Muestra_EquiposTelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLidia2 = New sofTV.DataSetLidia2
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Muestra_EquiposTelTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_EquiposTelTableAdapter
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.CMDRadioButton2 = New System.Windows.Forms.RadioButton
        Me.CMBRadioButton1 = New System.Windows.Forms.RadioButton
        Me.CMBLabel3 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Muestra_Paquetes_AdicBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_Paquetes_AdicTableAdapter = New sofTV.DataSetLidia2TableAdapters.Muestra_Paquetes_AdicTableAdapter
        Label1 = New System.Windows.Forms.Label
        CType(Me.Muestra_EquiposTelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.Muestra_Paquetes_AdicBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.BackColor = System.Drawing.Color.Transparent
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.OrangeRed
        Label1.Location = New System.Drawing.Point(15, 13)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(246, 18)
        Label1.TabIndex = 52
        Label1.Text = "Seleccionar Número Telefónico"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label3.Location = New System.Drawing.Point(18, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(328, 18)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "Seleccionar Número Telefónico de Linea 2"
        Me.Label3.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label2.Location = New System.Drawing.Point(4, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(339, 18)
        Me.Label2.TabIndex = 56
        Me.Label2.Text = "Seleccionar Paquete Adicional para la Mac :"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.Muestra_EquiposTelBindingSource
        Me.ComboBox1.DisplayMember = "MACCABLEMODEM"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(15, 37)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(372, 23)
        Me.ComboBox1.TabIndex = 53
        Me.ComboBox1.ValueMember = "Clv_Equipo"
        '
        'Muestra_EquiposTelBindingSource
        '
        Me.Muestra_EquiposTelBindingSource.DataMember = "Muestra_EquiposTel"
        Me.Muestra_EquiposTelBindingSource.DataSource = Me.DataSetLidia2
        '
        'DataSetLidia2
        '
        Me.DataSetLidia2.DataSetName = "DataSetLidia2"
        Me.DataSetLidia2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(7, 153)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(10, 33)
        Me.Button5.TabIndex = 54
        Me.Button5.Text = "&CANCELAR"
        Me.Button5.UseVisualStyleBackColor = False
        Me.Button5.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(277, 157)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 33)
        Me.Button1.TabIndex = 55
        Me.Button1.Text = "&ACEPTAR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Muestra_EquiposTelTableAdapter
        '
        Me.Muestra_EquiposTelTableAdapter.ClearBeforeFill = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.ComboBox3)
        Me.Panel1.Controls.Add(Label1)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Location = New System.Drawing.Point(7, 9)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(408, 135)
        Me.Panel1.TabIndex = 56
        '
        'ComboBox3
        '
        Me.ComboBox3.DataSource = Me.Muestra_EquiposTelBindingSource
        Me.ComboBox3.DisplayMember = "MACCABLEMODEM"
        Me.ComboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(18, 95)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(372, 23)
        Me.ComboBox3.TabIndex = 55
        Me.ComboBox3.ValueMember = "Clv_Equipo"
        Me.ComboBox3.Visible = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.CMDRadioButton2)
        Me.Panel3.Controls.Add(Me.CMBRadioButton1)
        Me.Panel3.Controls.Add(Me.CMBLabel3)
        Me.Panel3.Location = New System.Drawing.Point(3, 9)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(415, 99)
        Me.Panel3.TabIndex = 57
        '
        'CMDRadioButton2
        '
        Me.CMDRadioButton2.AutoSize = True
        Me.CMDRadioButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMDRadioButton2.Location = New System.Drawing.Point(206, 66)
        Me.CMDRadioButton2.Name = "CMDRadioButton2"
        Me.CMDRadioButton2.Size = New System.Drawing.Size(185, 20)
        Me.CMDRadioButton2.TabIndex = 54
        Me.CMDRadioButton2.TabStop = True
        Me.CMDRadioButton2.Text = "Seleccionar de la Lista"
        Me.CMDRadioButton2.UseVisualStyleBackColor = True
        '
        'CMBRadioButton1
        '
        Me.CMBRadioButton1.AutoSize = True
        Me.CMBRadioButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBRadioButton1.Location = New System.Drawing.Point(50, 66)
        Me.CMBRadioButton1.Name = "CMBRadioButton1"
        Me.CMBRadioButton1.Size = New System.Drawing.Size(103, 20)
        Me.CMBRadioButton1.TabIndex = 53
        Me.CMBRadioButton1.TabStop = True
        Me.CMBRadioButton1.Text = "Automática"
        Me.CMBRadioButton1.UseVisualStyleBackColor = True
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.BackColor = System.Drawing.Color.Transparent
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.ForeColor = System.Drawing.Color.OrangeRed
        Me.CMBLabel3.Location = New System.Drawing.Point(19, 13)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(262, 18)
        Me.CMBLabel3.TabIndex = 52
        Me.CMBLabel3.Text = "Asignación de Número Telefónico"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(8, 232)
        Me.TextBox1.MaxLength = 10
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(216, 20)
        Me.TextBox1.TabIndex = 53
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.ComboBox2)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(8, 12)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(389, 135)
        Me.Panel2.TabIndex = 57
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.OrangeRed
        Me.Label4.Location = New System.Drawing.Point(4, 42)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(97, 18)
        Me.Label4.TabIndex = 57
        Me.Label4.Text = "Seleccionar"
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.Muestra_Paquetes_AdicBindingSource
        Me.ComboBox2.DisplayMember = "Descripcion"
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(7, 92)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(372, 23)
        Me.ComboBox2.TabIndex = 56
        Me.ComboBox2.ValueMember = "clv_Paqadi"
        '
        'Muestra_Paquetes_AdicBindingSource
        '
        Me.Muestra_Paquetes_AdicBindingSource.DataMember = "Muestra_Paquetes_Adic"
        Me.Muestra_Paquetes_AdicBindingSource.DataSource = Me.DataSetLidia2
        '
        'Muestra_Paquetes_AdicTableAdapter
        '
        Me.Muestra_Paquetes_AdicTableAdapter.ClearBeforeFill = True
        '
        'FrmSeleccionaTel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(425, 202)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmSeleccionaTel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Télefono"
        Me.TopMost = True
        CType(Me.Muestra_EquiposTelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLidia2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.Muestra_Paquetes_AdicBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataSetLidia2 As sofTV.DataSetLidia2
    Friend WithEvents Muestra_EquiposTelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_EquiposTelTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_EquiposTelTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Muestra_Paquetes_AdicBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Paquetes_AdicTableAdapter As sofTV.DataSetLidia2TableAdapters.Muestra_Paquetes_AdicTableAdapter
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CMDRadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents CMBRadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
